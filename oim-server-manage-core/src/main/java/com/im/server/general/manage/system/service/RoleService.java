package com.im.server.general.manage.system.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.im.server.general.common.bean.system.Role;
import com.im.server.general.common.bean.system.RoleMenu;
import com.im.server.general.common.dao.system.RoleDAO;
import com.im.server.general.common.dao.system.RoleMenuDAO;
import com.im.server.general.common.dao.system.UserRoleDAO;
import com.im.server.general.common.data.system.RoleQuery;
import com.onlyxiahui.query.hibernate.QueryWrapper;
import com.onlyxiahui.query.hibernate.util.QueryUtil;
import com.onlyxiahui.query.page.QueryPage;



/**
 * 
 * date 2018-07-12 11:15:01<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@Service
@Transactional
public class RoleService {

	@Resource
	RoleDAO roleDAO;
	@Resource
	RoleMenuDAO roleMenuDAO;
	@Resource
	UserRoleDAO userRoleDAO;

	public Role getRole(String id) {
		id = (null == id) ? "" : id;
		return roleDAO.get(id);
	}

	public void addOrUpdate(Role role, List<String> menuIds) {
		if (null == role.getId() || "".equals(role.getId())) {
			role.setCreateTime(new Date());
			roleDAO.save(role);
		} else {
			roleDAO.updateSelective(role);
		}
		String roleId = role.getId();
		addOrUpdate(roleId, menuIds);
	}

	public void addOrUpdate(String roleId, List<String> menuIds) {
		List<RoleMenu> rmList = roleMenuDAO.getRoleMenuListByRoleId(roleId);

		List<RoleMenu> deleteList = new ArrayList<RoleMenu>();
		Map<String, RoleMenu> map = new HashMap<String, RoleMenu>();
		for (RoleMenu rm : rmList) {
			if (map.containsKey(rm.getMenuId())) {
				deleteList.add(rm);
			}
			map.put(rm.getMenuId(), rm);
		}

		List<RoleMenu> addList = new ArrayList<RoleMenu>();
		if (null != menuIds) {
			for (String menuId : menuIds) {
				RoleMenu rm = map.remove(menuId);
				if (null == rm) {
					rm = new RoleMenu();
					rm.setRoleId(roleId);
					rm.setMenuId(menuId);
					addList.add(rm);
				}
			}
		}
		deleteList.addAll(map.values());
		if (!addList.isEmpty()) {
			for (RoleMenu rm : addList) {
				roleMenuDAO.save(rm);
			}
		}
		if (!deleteList.isEmpty()) {
			for (RoleMenu rm : deleteList) {
				roleMenuDAO.delete(rm.getId());
			}
		}
	}

	public void delete(String id) {
		roleDAO.delete(id);
		roleMenuDAO.deleteByRoleId(id);
		userRoleDAO.deleteByRoleId(id);
	}

	public List<Role> queryList(RoleQuery roleQuery, QueryPage queryPage) {
		QueryWrapper map = QueryUtil.getQueryWrapper(roleQuery);
		map.setPage(queryPage);
		List<Role> roleList = roleDAO.queryList(map);
		return roleList;
	}

	public List<Role> allList() {
		QueryWrapper map = new QueryWrapper();
		List<Role> roleList = roleDAO.queryList(map);
		return roleList;
	}

	public List<RoleMenu> getRoleMenuListByRoleId(String roleId) {
		List<RoleMenu> rmList = roleMenuDAO.getRoleMenuListByRoleId(roleId);
		return rmList;
	}

}
