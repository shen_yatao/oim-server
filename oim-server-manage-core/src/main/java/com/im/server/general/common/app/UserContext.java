package com.im.server.general.common.app;

/**
 * date 2018-07-16 17:03:37<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
public class UserContext {

	private String currentUserId;
	private String token;

	public String getCurrentUserId() {
		return currentUserId;
	}

	public void setCurrentUserId(String currentUserId) {
		this.currentUserId = currentUserId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
