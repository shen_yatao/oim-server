package com.im.server.general.common.dao.system;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.im.base.dao.BaseDAO;
import com.im.server.general.common.bean.system.Menu;
import com.im.server.general.common.bean.system.UserRole;
import com.onlyxiahui.query.hibernate.QueryWrapper;



/**
 * 
 * date 2018-07-12 11:00:00<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@Repository
public class UserMenuDAO extends BaseDAO {

	String namespace = UserRole.class.getName();

	public List<Menu> getMenuListByUserId(String userId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		
		StringBuilder getRoleId = new StringBuilder();
		getRoleId.append(" select ur.roleId from ");
		getRoleId.append(" m_user_role ");
		getRoleId.append(" ur where ur.userId ='");
		getRoleId.append(userId);
		getRoleId.append("' ");

		StringBuilder getMenuId = new StringBuilder();
		getMenuId.append(" select rm.menuId from ");
		getMenuId.append(" m_role_menu ");
		getMenuId.append(" rm where rm.roleId in ( ");
		getMenuId.append(getRoleId);
		getMenuId.append(" ) ");

		StringBuilder getMenuList = new StringBuilder();
		getMenuList.append("select m.* from ");
		getMenuList.append(" m_menu m where 1=1 ");
		getMenuList.append(" and flag =");
		getMenuList.append(Menu.flag_enable);
		
		getMenuList.append(" and m.id in( ");
		getMenuList.append(getMenuId);
		getMenuList.append(" ) ");

		List<Menu> list = readDAO.queryList(getMenuList.toString(),queryWrapper, Menu.class,null);
		return list;
	}
}
